// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");

// Khởi tạo app express
const app = express();

const router = express.Router();

// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

// const {users} = require("./data");
const userRouter = require("./app/routes/userRouter");


app.use((req, res, next) => {
 let today = new Date();

 console.log("Current: ", today);

 next();
});

app.use((req, res, next) => {
 console.log("Method: ", req.method);

 next();
});

// Khai báo API
app.get("/", (req, res) => {
 const today = new Date();
 res.status(200).json({
  message:`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
 })
})

app.use(userRouter);

// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
 console.log("App listening on port: ", port);
});


