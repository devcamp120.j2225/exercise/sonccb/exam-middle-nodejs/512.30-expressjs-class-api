const express = require("express");

const router = express.Router();

const {users} = require("../../data");
const {userMiddleware} = require("../MIddlewares/userMiddleware");

router.use(userMiddleware);

router.get("/users", (req,res) => {
 let age = req.query.age;
 if (!age) {
  return res.status(200).json({
   message:`Get all user successfully!`,
   users
  })
 } else{
  matchUser = users.filter((a,b) =>{
   return a.age > age
  });
  return res.status(200).json({
   message:`Get user by age:${age} successfully!`,
   users:matchUser
  })
 }
 
});

router.get("/users/:userId", (req,res) => {
 let userId = req.params.userId;
 let matchUser = users.filter((a,b) =>{
  return a.id == userId
 })
 res.status(200).json({
  message:`Get user by id:${userId} successfully!`,
  user:matchUser[0]
 })
});

//create a user
router.post('/users', (req, res) => {
 let body = req.body;

 console.log('create a user');
 console.log(body);
 res.status(201).json({
    message:"Create user succesfully!",
    data:body
 })
});

//update a user
router.put('/users/:userId', (req, res) => {
 let id = req.params.userId;
 let body = req.body;

 res.status(200).json({
     message:`update user id ${id} successfully!`,
     data:body
 })
})

//delete a user
router.delete('/users/:userId', (req, res) => {
 let id = req.params.userId;

 res.status(204).json({
     message: `Delete user id ${id} successfully!`
 })
})



module.exports = router;